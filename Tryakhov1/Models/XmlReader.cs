﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Tryakhov1.Models
{
    public class XmlReader
    {
        /// <summary>  
        /// Return list of products from XML.  
        /// </summary>  
        /// <returns>List of products</returns>  
        public static IEnumerable<Parameters> RetrunListOfParameters()
        {
            var xmlData = HttpContext.Current.Server.MapPath("~/App_Data/Input.xml");//Path of the xml script  
            var ds = new DataSet();//Using dataset to read xml file  
            ds.ReadXml(xmlData);
            var parameters = (from rows in ds.Tables[0].AsEnumerable()
                select new Parameters
                {
                    ParameterId = rows[0].ToString(), //Convert row to int  
                    ParameterName = rows[1].ToString(),
                    ParameterDescription = rows[2].ToString(),
                    ParameterType = rows[3].ToString(),
                    ParameterValue = rows[4].ToString()
                }).ToList();
            return parameters;
        }

        public void SetValueOfParameter(string parameterName, string parameterValue)
        {
            var xmlData = HttpContext.Current.Server.MapPath("~/App_Data/Input.xml");//Path of the xml script  
            var ds = new DataSet();//Using dataset to read xml file  
            ds.WriteXml(xmlData);
            ds.Tables[0].Columns[parameterName].Caption = parameterValue;
        }
    }
}
