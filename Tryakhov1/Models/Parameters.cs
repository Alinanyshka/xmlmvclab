﻿using System;
using System.Xml.Serialization;

namespace Tryakhov1.Models
{
    [Serializable]
    [XmlRoot("Parameters"), XmlType("Parameters")]
    public class Parameters
    {
        public string ParameterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterDescription { get; set; }
        public string ParameterType { get; set; }
        public string ParameterValue { get; set; }
    }
}
