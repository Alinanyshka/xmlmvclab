﻿using System.Linq;
using System.Web.Mvc;
using Tryakhov1.Models;

namespace Tryakhov1.Controllers
{
    public class SettingsController : Controller
    {
        public ActionResult Settings()
        {
            var xmlReader = new XmlReader();
            return View(XmlReader.RetrunListOfParameters().ToList());
        }
    }
}